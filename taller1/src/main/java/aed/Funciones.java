package aed;

class Funciones {
    int cuadrado(int x) {
        // COMPLETAR
        return x * x;
    }

    double distancia(double x, double y) {
        // COMPLETAR
        return Math.sqrt( x * x + y * y);
    }

    boolean esPar(int n) {
        // COMPLETAR
        
        return n % 2 == 0;
    }

    boolean esBisiesto(int n) {
        // COMPLETAR
        return (n % 4 == 0 && (n % 100 != 0 || n% 400 == 0));
    }

    int factorialIterativo(int n) {
        // COMPLETAR
        int i = 0;
        if (n >= 0){
            i = 1;
        for(int x = 1; x <= n; x+=1){
            i = i * x;
        }
        }
        
        return i;
    }

    int factorialRecursivo(int n) {
        // COMPLETAR
        if (n==0) return 1;
        return factorialRecursivo(n-1) * n;
    }

    boolean esPrimo(int n) {
        // COMPLETAR
        int divisores = 0;
        for(int i = 1; i <= n; i+=1){
            if (n % i == 0){
                divisores +=1;
            }
        }
        return divisores == 2;
    }

    int sumatoria(int[] numeros) {
        // COMPLETAR
        int res = 0;
        for(int i = 0; i< numeros.length; i+=1){
            res = res + numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        // COMPLETAR
        for(int i = 0; i<= numeros.length; i+=1){
            if(numeros[i] == buscado) return i;
        }
        return 0;
    }

    boolean tienePrimo(int[] numeros) {
        // COMPLETAR
        for(int i = 0; i< numeros.length; i+=1){
            if(esPrimo(numeros[i]))return true;
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        // COMPLETAR
        for(int i = 0; i< numeros.length; i+=1){
            if(!esPar(numeros[i])) return false;
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        // COMPLETAR
        if (s1.length() > s2.length()) return false;
        for(int i = 0; i< s1.length(); i+=1){
            if(s1.charAt(i) != s2.charAt(i)) return false;
        }
        return true;
    }

    boolean esSufijo(String s1, String s2) {
        // COMPLETAR
        if (s1.length() > s2.length())return false;
        for(int i = 1; i <= s1.length();  i+=1){
            if(s1.charAt(s1.length() - i) != s2.charAt(s2.length() - i)) return false;
        }
        return true;
    }
}
