package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] vector = new float[largo];
        for (int i = 0; i < largo; i+=1) {
            vector[i] = entrada.nextFloat();
        }
        return vector;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] matriz = new float[filas][columnas];
        for (int i = 0; i<filas; i+=1){
            matriz[i] = leerVector(entrada, columnas);

        }
        return matriz;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        int largo= 0;
        if(alto > 0){
            largo = 1 + (2*(alto - 1));
        }
        for(int i = 1; i<=largo; i+=2){
            for(int repetir = 1; repetir <= ((largo -i) / 2); repetir +=1){
                salida.print(" ");
            }
            for(int repetir = 1; repetir <= i; repetir +=1){
                salida.print("*");
            }
            for(int repetir = 1; repetir <= ((largo -i) / 2) ; repetir +=1){
                salida.print(" ");
            }
            salida.print("\n");
    }
    }
    
}
