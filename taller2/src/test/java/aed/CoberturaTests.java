package aed;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class CoberturaTests {
    Cobertura cobertura = new Cobertura();

    @Test
    void testFizzBuzz() {
        assertEquals("FizzBuzz",cobertura.fizzBuzz(30));
        assertEquals("Fizz",cobertura.fizzBuzz(18));
        assertEquals("Buzz",cobertura.fizzBuzz(25));
        assertEquals("8",cobertura.fizzBuzz(8));
    }

    @Test
    void testNumeroCombinatorio() {
        assertEquals(1,cobertura.numeroCombinatorio(10,0));
        assertEquals(1,cobertura.numeroCombinatorio(0,0));
        assertEquals(1,cobertura.numeroCombinatorio(5,5));
        assertEquals(56,cobertura.numeroCombinatorio(8,5));
        assertEquals(0,cobertura.numeroCombinatorio(4,9));
    }

    @Test
    void testRepeticionesConsecutivas() {
        assertEquals(0,cobertura.repeticionesConsecutivas(new int[0]));
        assertEquals(1,cobertura.repeticionesConsecutivas(new int[]{1,6,7,2}));
        assertEquals(3,cobertura.repeticionesConsecutivas(new int[]{1,2,2,2,5,7}));
        assertEquals(4,cobertura.repeticionesConsecutivas(new int[]{1,2,2,2,5,7,7,7,7,4}));
        assertEquals(4,cobertura.repeticionesConsecutivas(new int[]{1,1,2,2,3,3,3,4,4,4,4,5}));
        assertEquals(9,cobertura.repeticionesConsecutivas(new int[]{9,9,9,9,9,9,9,9,9}));
    }
}
